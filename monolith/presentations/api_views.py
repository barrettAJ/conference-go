import json
import pika
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Presentation, Status
from events.models import Conference
from common.json import ModelEncoder


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["PUT"])
def api_approve_presentation(request, id):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_approvals")
    presentation = Presentation.objects.get(id=id)
    fields = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }
    body_content = json.dumps(fields)
    channel.basic_publish(
        exchange="", routing_key="presentation_approvals", body=body_content
    )
    connection.close()

    presentation.approve()
    return JsonResponse(
        presentation, encoder=PresentationDetailEncoder, safe=False
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, id):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_rejections")
    presentation = Presentation.objects.get(id=id)
    fields = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }
    body_content = json.dumps(fields)
    channel.basic_publish(
        exchange="", routing_key="presentation_rejections", body=body_content
    )
    presentation.reject()
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filder(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = Conference.objects.get(id=content["conference"])
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse({"message": "Invalid conference"}, status=400)
        presentation = Presentation.objects.filter(id=id).update(**content)
        return JsonResponse(
            presentation, encoder=PresentationDetailEncoder, safe=False
        )


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        presentations = Presentation.objects.all()
        return JsonResponse(
            presentations, encoder=PresentationListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Status.DoesNotExist:
            return JsonResponse({"message": "Invalid conference"}, status=400)
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation, encoder=PresentationListEncoder, safe=False
        )


# or
# from json import JSONEncoder
# class ConferenceEndcoder(JSONEncoder):
#   properties = ['name','description']
#   def default(self, conference)
#       d = {}
#       for property in self.properties:
#             value = getattr(conference, property)
# d[property]= value
#       return d


# in api show presentation
# return JsonResponse(confence, encoder=ConferenceEncoder, safe=False)


# in common> json.py
# class ModelEncoder(JSONEncoder):
#   def default(self, o):
#       d = {}
#       for property in self.properties:
#           value = getattr(o, property)
#           d[property] = value
#       return d

import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_picture(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": f"{city}, {state}"}
    response = requests.get(
        "https://api.pexels.com/v1/search", headers=headers, params=params
    )
    content = json.loads(response.content)
    try:
        return content["photos"][0]["url"]
    except:
        pass


def get_geo(city, state):
    response = requests.get(
        f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&appid={OPEN_WEATHER_API_KEY}"
    )
    content = json.loads(response.content)

    if content == []:
        lat = 0
        lon = 0
    else:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    return lat, lon


def get_weather(city, state):
    lat, lon = get_geo(city, state)
    response = requests.get(
        f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    )
    content = json.loads(response.content)
    temp = content["main"]["temp"]
    description = content["weather"][0]["description"]
    weather = {"weather": {"temp": temp, "description": description}}
    return weather
